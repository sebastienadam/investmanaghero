from flask import Blueprint, render_template

bp = Blueprint('indices', __name__, url_prefix='/indices')

@bp.route('/', methods=('GET', 'POST'))
def indices_list():
    return render_template('indices/list.html')