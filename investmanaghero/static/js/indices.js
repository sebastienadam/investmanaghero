Vue.createApp({
    created() {
        this.fetchData()
    },
    data() {
        return {
            action : null,
            currentIndice : {
                id : null,
                yyyymm : null,
                value: null
            },
            error : false,
            indices: null,
        }
    },
    methods: {
        fetchData() {
            axios.get(urlsApi.list)
            .then(response => {
                // handle success
                // console.log(response.data);
                this.indices = response.data;
            })
            .catch(error => {
                // handle error
                console.log(error);
            })
        },
        hideModal() {
            document.getElementById('modalIndiceForm').style.display='none';
            this.error = false;
        },
        processModalForm() {
            // console.log('Form content:');
            // console.log('yyyymm: '+this.currentIndice.yyyymm+' ('+typeof(this.currentIndice.yyyymm)+')');
            // console.log('value: '+this.currentIndice.value+' ('+typeof(this.currentIndice.yyyymm)+')');
            var url;
            if(this.action === 'create') {
                url = urlsApi.create;
            } else if (this.action === 'update') {
                url = urlsApi.update;
            } else {
                url = null;
            }
            axios.post(url, this.currentIndice)
            .then(response => {
                // handle success
                // console.log(response.data);
                this.fetchData();
                this.hideModal();
            })
            .catch(error => {
                // handle error
                this.error = true;
                console.log(error);
            })
        },
        showModal(indice = null) {
            if(indice === null) {
                this.action = 'create';
                this.currentIndice = {
                    id : null,
                    yyyymm : null,
                    value: null
                };
            } else {
                this.action = 'update';
                this.currentIndice = indice;
            }
            document.getElementById('modalIndiceForm').style.display='block';
            console.log('Form action: '+this.action);
            console.log('Current indice: '+JSON.stringify(this.currentIndice));
        }
    },
}).mount('#app')