from flask import current_app, g
from sqlalchemy import create_engine, MetaData, Table


class DB():
    """
    Class to save db connection parameters
    """

    def __init__(self):
        self.engine = None
        self.conn = None
        self.metadata = None
    
    def get_table(self, name):
        """ Retrive a table by his name """
        return Table(name, self.metadata, autoload=True, autoload_with=self.engine)
    
    def execute(self, statment):
        """ Shortcut to execute statment """
        if self.conn is not None:
            return self.conn.execute(statment)

    def close(self):
        if self.conn is not None:
            self.conn.close()
        if self.engine is not None:
            self.engine.dispose()

def get_db():
    """
    Retrieve the DB connection if exists, otherwise create it
    """
    if 'db' not in g:
        g.db = DB()
        g.db.engine = create_engine(current_app.config['DATABASE_URI'], echo=False)
        g.db.conn = g.db.engine.connect()
        g.db.metadata = MetaData()
    return g.db


def close_db(e=None):
    """
    Close connection to the DB
    """
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_app(app):
    """
    Configure app to close automatically connection to DB when stopping
    """
    app.teardown_appcontext(close_db)