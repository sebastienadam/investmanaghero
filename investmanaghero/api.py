from flask import Blueprint, jsonify, request
from .models import Indice
from .models import Indices

bp = Blueprint('api', __name__, url_prefix='/api')

@bp.route('/indices', methods=('GET', 'POST'))
def indices_list():
    indices = Indices()
    result = [{key:value for (key, value) in indice.items()} for indice in indices.list()]
    return jsonify(result)

@bp.route('/indices/create', methods=('POST',))
def indice_create():
    input_data = request.json
    new_indice = Indice()
    try:
        new_indice.create(**input_data)
        http_code = 200
        resp = {'id' : new_indice.id}
    except Exception as ex:
        http_code = 406
        resp = {'msg' : f"{type(ex)}: {str(ex)}"}
    return jsonify(resp), http_code

@bp.route('/indices/update', methods=('POST',))
def indice_update():
    input_data = request.json
    print(input_data)
    current_indice = Indice()
    try:
        current_indice.load(input_data['id'])
        [setattr(current_indice, key, input_data.get(key)) for key in current_indice.as_dict().keys() if key != 'id']
        current_indice.save()
        http_code = 200
        resp = {'id' : current_indice.id}
    except Exception as ex:
        http_code = 406
        resp = {'msg' : f"{type(ex)}: {str(ex)}"}
        raise
    return jsonify(resp), http_code
