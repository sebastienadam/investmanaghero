from .. import db

from sqlalchemy import desc

class Indices():
    """ load indices list """

    def __init__(self):
        self._db = db.get_db()
        self._filters = {}
        self._table = self._db.get_table('indices')

    def _apply_filters(self, stm_select):
        table = self._table
        return stm_select

    def list(self, **filters):
        if filters:
            self.set_fiters(filters)
        table = self._table
        stm_select = table.select().order_by(desc(table.c.yyyymm))
        stm_select = self._apply_filters(stm_select)
        return self._db.execute(stm_select).fetchall()

    def set_fiters(self, filters):
        self._filters = filters