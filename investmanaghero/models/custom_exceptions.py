"""
The exceptions defined here are intended to explain the errors encountered by
the models.
"""

class IntegrityError(Exception):
    pass


class NoDataFoundError(Exception):
    pass