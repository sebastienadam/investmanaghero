from .. import db
from .custom_exceptions import IntegrityError
from .custom_exceptions import NoDataFoundError

import sqlalchemy

class Indice():
    """ load indice """

    def __init__(self):
        self._db = db.get_db()
        self._table = self._db.get_table('indices')
        self._reset_data()

    def _a_prop_set_value(self, key, value):
        if value is not None and value != '':
            self._data[key] = value

    def _a_prop_id_get(self):
        return self._data.get('id')

    id = property(_a_prop_id_get)

    def _a_prop_value_get(self):
        return self._data.get('value')

    def _a_prop_value_set(self, value):
        self._a_prop_set_value('value', float(value))

    value = property(_a_prop_value_get, _a_prop_value_set)

    def _a_prop_yyyymm_get(self):
        return self._data.get('yyyymm')

    def _a_prop_yyyymm_set(self, value):
        self._a_prop_set_value('yyyymm', int(value))

    yyyymm = property(_a_prop_yyyymm_get, _a_prop_yyyymm_set)

    def _reset_data(self):
        """ Initialize indice data """
        self._data = {key:None for key in self._table.c.keys()}

    def as_dict(self):
        return self._data

    def create(self, **kwargs):
        """
        Create a new indice, and then, if everything is in order, load it.
        """
        data = {key:kwargs.get(key) for key in self._data.keys() if key != 'id'}
        if not all([data['value'], data['yyyymm']]):
            raise AttributeError('Value and yyyymm are required.')
        try:
            stmt_insert = self._table.insert(data)
            result = self._db.conn.execute(stmt_insert).inserted_primary_key[0]
            self.load(id_=result)
            return True
        except sqlalchemy.exc.IntegrityError as ex:
            raise IntegrityError(ex)

    def load(self, id_):
        """
        Loads a indice's data from the database. The indice can be retrieved
        based on his ID.
        """
        stmt_select = self._table.select().where(self._table.c.id == id_)
        result = self._db.conn.execute(stmt_select).fetchone()
        if result is None:
            self._reset_data()
            raise NoDataFoundError()
        self._data = {key:value for (key, value) in result.items()}

    def save(self):
        """ Save current indice into database """
        if self.id is None:
            raise AttributeError('You must first load the indice to be able to save it.')
        data = {key:self._data[key] for key in self._data.keys() if key not in ['id']}
        stmt_update = self._table.update().values(data).where(self._table.c.id == self.id)
        result = self._db.conn.execute(stmt_update).rowcount
        if result == 1:
            return True
        elif result == 0:
            self._reset_data()
            raise ValueError('No data were saved')
        else:
            RuntimeError('Too many records have been updated')
