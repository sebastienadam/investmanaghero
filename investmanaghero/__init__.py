from flask import Flask, render_template
from pathlib import Path

from . import api
from . import db
from . import indices

def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # ensure the instance folder exists
    Path(app.instance_path).mkdir(parents=True, exist_ok=True)

    # Load config
    app.config.from_pyfile('app.cfg', silent=True)

    # Initialise db
    db.init_app(app)

    # Load modules
    app.register_blueprint(api.bp)
    app.register_blueprint(indices.bp)

    # temporary main page
    @app.route('/')
    def index():
        return render_template('common/index.html')

    return app