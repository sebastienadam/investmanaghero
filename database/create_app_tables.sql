-- indices
create table indices (
    id serial primary key,
    yyyymm int not null unique,
    value numeric not null
);
